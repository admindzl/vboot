<div align=center>

![输入图片说明](https://images.gitee.com/uploads/images/2018/0812/134526_37264db5_82.png "logo.png")

</div>

### vboot是一个 Vue 和 SpringBoot2.0的组合基础工程。如果你喜欢使用这两个框架做前后台开发，而又不知道如何让它们很好的组合，那么这个项目可能会是你入手学习的一个很好选择。

![输入图片说明](https://images.gitee.com/uploads/images/2018/1227/120434_e2fd700b_82.png)

## VBoot主要依赖四大框架.
- [Vue 2.x](https://cn.vuejs.org)
- [SpringBoot 2.x](http://spring.io/projects/spring-boot)
- [AdminLTE 2.x](https://adminlte.io/themes/AdminLTE/index2.html)
- [vx-easyui](https://www.jeasyui.com/documentation4/index.php)

## 前端插件的使用
*   [vue-select](http://sagalbot.github.io/vue-select/docs)
*   [bootstrapvalidator](http://bootstrapvalidator.votintsev.ru)
*   [Qs](https://github.com/ljharb/qs)
*   [vue-snotify](https://artemsky.github.io/vue-snotify/documentation/essentials/getting-started.html)
*   [axios](https://github.com/axios/axios)
*   [font-awesome](https://fontawesome.com)
*   [lodash](https://lodash.com)
*   [pretty-checkbox-vue](https://hamed-ehtesham.github.io/pretty-checkbox-vue)
*   [vue-cropper](https://github.com/xyxiao001/vue-cropper)

## 项目结构说明

项目后台构建使用gradle，前端构建使用webpack。项目目录结构是这两者的综合体，config、static和 wbuild 是 webpack 构建的配置。前后台代码都在 src 目录下，front目录表示所有的前端代码，main 保留了原始的标准 java 项目结构。

前端依赖配置为package.json
后台依赖配置为build.gradle

## 初始化项目前端依赖

```
npm install
```
or

```
yarn install
```
## 导入开发工具，建议使用idea
一般 idea 导入 gradle 项目都会进行自动构建和下载依赖，如果没有，在 idea 的右边栏有 gradle 的工具类，点击![输入图片说明](https://images.gitee.com/uploads/images/2018/0810/165054_feed6e88_82.png "屏幕截图.png")刷新按钮即可。首次初始化，可能会需要比较长时间的下载，需要耐心等待。

## 数据库和数据源配置
基础数据在 vboot.sql中，自行创建好数据库导入数据即可。

数据源的修改则在application.yml中对应配置即可，对于熟悉springboot的你，这都不是问题。

## 运行项目
后台启动项配置：
![输入图片说明](https://images.gitee.com/uploads/images/2018/0810/165556_1bcbb2c6_82.png "屏幕截图.png")

前台启动项配置：
![输入图片说明](https://images.gitee.com/uploads/images/2018/0810/165643_3526cb13_82.png "屏幕截图.png")

前端编译：
![输入图片说明](https://images.gitee.com/uploads/images/2018/0810/165707_78313121_82.png "屏幕截图.png")

#### 启动说明：
后台服务端口配置在application.yml的server.port默认配置8088，如果是开发模式下，只需启动服务即可，不需要使用浏览器访问。如果是准备发布，则需先执行如上配置的 front-build，再启动项目访问即可。

开发模式需要同时启动 front 服务和 boot 项目。 front 前端端口为8081。启动 front 服务后，访问 `http://localhost:8081/`,开发时所有的数据请求都会被代理到后台 boot 端进行处理。build 发布后自动能够无缝切换环境。

![输入图片说明](https://images.gitee.com/uploads/images/2018/0810/170356_cccf7b52_82.png "屏幕截图.png")

## front 目录结构说明
![输入图片说明](https://images.gitee.com/uploads/images/2018/0810/170435_fbf3b8ae_82.png "屏幕截图.png")


如果你熟悉 vue 开发，那么这个结构你应该不会太陌生。

- `api` 数据接口的配置和接口访问规则定义
- `assets` 不需要经常改变的静态资源
- `components` 自定义的全局组件
- `directive` 自定义指令，注意是定义了权限控制指令
- `filters`  全局的过滤器，这个暂时没用到，功能少，预留的标准结构。
- `router` 前端的路由配置
- `store` 全局数据状态管理 
- `styles` 样式，app.scss 为全局样式
- `utils` 工具包
- `views` 所有的页面都在此处了
- `App.vue` vue 程序的主界面
- `bootstrap.js` 一些全局的加载项和配置项
- `main.js` 前端入口 js
- `mixins.js` vue组件的全局配置。

## 项目截图

![输入图片说明](https://images.gitee.com/uploads/images/2018/0810/174554_7ea8f156_82.gif "在这里输入图片标题")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0810/174513_4cf46800_82.gif "在这里输入图片标题")